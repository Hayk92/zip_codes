import HTTP from '../http-common';

const countries = (data = null) => {
    return HTTP.get(`/countries/`, {
        params: data
    });
};

export const countryService = {
    countries
};
