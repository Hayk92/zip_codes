import HTTP from '../http-common';

const places = (data = null) => {
    return HTTP.get(`/places/`, {
        params: data
    });
};

export const placeService = {
    places
};

