import Vue from 'vue'
import App from './App.vue'
import router from './router'
import {store} from './_store'
import './registerServiceWorker'
import VeeValidate from 'vee-validate';
import {helperMixin} from './mixinns';

import * as VueGoogleMaps from 'vue2-google-maps'

Vue.config.productionTip = false;
Vue.use(VeeValidate);

Vue.mixin(helperMixin);

Vue.use(VueGoogleMaps, {
    load : {
        key: process.env.VUE_APP_GOOGLE_API_KEY,
        libraries: 'places'
    }
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
