import axios from 'axios'

const HTTP = axios.create({
    baseURL: process.env.VUE_APP_API_BASE_URL
});

HTTP.interceptors.request.use(
    config => {
        return config;
    },
    error => Promise.reject(error)
);

export default HTTP;
