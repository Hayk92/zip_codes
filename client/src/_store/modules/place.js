export default {
    state: {
        places: []
    },
    mutations: {
        SET_PLACES(state, payload) {
            state.places = payload;
        }
    },
    actions: {
        SET_PLACES({dispatch, commit}, payload) {
            commit('SET_PLACES', payload);
        }
    },
    getters: {
        PLACES: (state) => {
            return state.places;
        }
    }
}