export default {
    state: {
        countries: []
    },
    mutations: {
        SET_COUNTRIES(state, payload) {
            state.countries = payload;
        }
    },
    actions: {
        SET_COUNTRIES({dispatch, commit}, payload) {
            commit('SET_COUNTRIES', payload);
        }
    },
    getters: {
        COUNTRIES: (state) => {
            return state.countries;
        }
    }
}