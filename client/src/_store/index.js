import Vue from 'vue';
import Vuex from 'vuex';

import country from './modules/country'
import place from './modules/place'

Vue.use(Vuex);

export const store = new Vuex.Store({
    modules: {
        country,
        place
    }
});
