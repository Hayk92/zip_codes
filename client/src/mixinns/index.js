import {mapGetters} from 'vuex';
export const helperMixin = {
    data: function () {
        return {
            map_center: { lat:40.1772093, lng:44.5090478 },
        }
    },
    methods: {
        showErrors(err, status) {
            if (status === 422) {
                console.log(err, status);
                Object.keys(err).map((key) => {this.errors.add({field: key, msg: err[key]})});
            }
        }
    },
    computed: {
        ...mapGetters(['PLACES']),
    },
};
