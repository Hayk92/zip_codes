<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('zip_code_id')->unsigned();
            $table->string('name');
            $table->string('country', 2);
            $table->string('state')->nullable();
            $table->string('state_code', 2)->nullable();
	        $table->decimal('lng', 11, 8);
	        $table->decimal('lat', 10, 8);
            $table->timestamps();

            $table->foreign('zip_code_id')->references('id')
	            ->on('zip_codes')
	            ->onUpdate('cascade')
	            ->onDelete('cascade');

            $table->index(['country']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('places');
    }
}
