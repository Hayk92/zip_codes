<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ZipCode extends Model
{
    protected $fillable = [
    	'zip_code'
    ];

    public function places() {
    	return $this->hasMany(Places::class, 'zip_code_id', 'id');
    }
}
