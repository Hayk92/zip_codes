<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\PlaceFinder;

class Places extends Model
{
	use PlaceFinder;

    protected $fillable = [
	    'zip_code_id',
		'name',
		'country',
		'state',
		'state_code',
		'lng',
		'lat',
    ];

	protected $appends = ['position', 'details'];

	public function zip_code() {
		return $this->belongsTo(ZipCode::class, 'zip_code_id', 'id');
	}

	public function getPositionAttribute()
	{
		return ['lat' => (float)$this->lat, 'lng' => (float)$this->lng];
	}

	public function getDetailsAttribute()
	{
		return false;
	}
}
