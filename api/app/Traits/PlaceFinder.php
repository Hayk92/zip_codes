<?php
namespace App\Traits;

use App\Models\ZipCode;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use League\Flysystem\Exception;

trait PlaceFinder
{
	public function getPlaces($country, $zip) {
		$places = self::where('places.country', $country)
			->join('zip_codes', function ($join) use ($zip){
				$join->on('zip_codes.id', 'places.zip_code_id')
					->where('zip_codes.zip_code', $zip);
			});

		if (!$places->count())
			$this->callApi($country, $zip);

		return $places->get();
	}

	private function callApi($country, $zip)
	{

		$places = [];

		try {
			$client = new \GuzzleHttp\Client();
			$url = env('ZIP_CODE_API')."/$country/$zip";
			$request = $client->request('GET', $url);
			$status = $request->getStatusCode();
			if($status == 200){
				$response = json_decode($request->getBody(), true);
				$zip_code = ZipCode::updateOrCreate(
					['zip_code' => $zip],
					['zip_code' => $zip]
				);
				foreach ($response['places'] as $place){
					$places[] = [
						"country" => $country,
						"zip_code_id" => $zip_code->id,
						"name" => $place['place name'] ?? null,
						"state" => $place['state'] ?? null,
						"state_code" => $place['state abbreviation'] ?? null,
						"lng" => $place['longitude'],
						"lat" => $place['latitude'],
					];
				}
				DB::table('places')->insert($places);

			}else{
				throw new \Exception('Failed');
			}

		} catch (\Exception $e) {
			$response = json_encode($e);
		}

		return $places;
	}
}